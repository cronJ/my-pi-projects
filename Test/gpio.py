#!/usr/bin/env python

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

# Ein Ausgang zum umschalten
GPIO.setup(16, GPIO.OUT)

# 10000 mal den Ausgang umschalten
print "Pin 16 umschalten..."

for i in range(1, 10000):
    status = 0
    if (status == 1):
        GPIO.output(16, GPIO.LOW)
        status = 0
    else:
        GPIO.output(16, GPIO.HIGH)
        status = 1
        
print "Fertig!"

GPIO.cleanup()